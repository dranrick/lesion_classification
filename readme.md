Óbudai Egyetem
Neumann János Informatikai kar
Mérökinformatikus szak, BSc nappali

Név: Hermann Ákos
Neptun: Z9K8RJ
Kozulens: Dr. Vámossy Zoltán

Anyajegyek képi melanóma-klasszifikációja mély neurális hálózatok segítségével
Image based lesion classification using deep neural networks

A projekt a fent feltüntetett hallgató szakdolgozatának részét képezi, a szakdolgozat dokumentumban taglalt rendszer implementációja.
Jelen repository egyaránt tartalmazza a klasszifikációs rendszert, az ahhoz szükséges python környzetet, valamint az adatokat eltöltő C# szoftvert is.
A projekthez tartozó dokumentációt maga a szakdolgozat jelenti, azonban az egyes modulok és kódrészletek külön kommentálva vannak, a jobb érthetőség érdekében. 


Az alábbi nyilvános linken néhány mintakép érhető el, nyers, feldolgozatlan formában:
 
https://drive.google.com/drive/folders/1AIsSoev3G_Weu4e14HnkmkpiPQkvIByz?usp=sharing
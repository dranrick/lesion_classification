﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDownloader
{
    public class SampleInfo
    {
        public string _id { get; set; }
        public Clinical clinical { get; set; }
        public string _modelType { get; set; }
    }
}

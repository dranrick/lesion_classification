﻿namespace SampleDownloader
{
    public class ImgData
    {
        public string _id { get; set; }

        public string Name { get; set; }

        public string Updated { get; set; }
    }
}
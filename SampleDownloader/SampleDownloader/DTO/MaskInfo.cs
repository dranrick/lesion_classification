﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDownloader
{
    public class MaskInfo
    {
        public string mole_id { get; set; }

        public SegmentationInfo segmentation { get; set; }
    }
}

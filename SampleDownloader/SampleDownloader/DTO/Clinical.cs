﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDownloader
{
    public class Clinical
    {
        public string age_approx { get; set; }
        public string anatom_site_general { get; set; }
        public string benign_malignant { get; set; }
        public string diagnosis { get; set; }
        public string diagnosis_confirm_type { get; set; }
        public string melanocytic { get; set; }
        public string sex { get; set; }
    }
}

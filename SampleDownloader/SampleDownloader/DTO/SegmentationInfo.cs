﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDownloader
{
    public class SegmentationInfo
    {
        public string _id { get; set; }

        public DateTime created { get; set; }

        public string failed { get; set; }

        public string skill { get; set; } 

        public string mole_id { get; set; }
    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SampleDownloader
{
    static class ISICHandler
    {
        public static List<string> UnsuccessfullIDs = new List<string>();

        public static string URL = "https://isic-archive.com/api/v1/image?limit=70000&sort=name&sortdir=1&detail=false";
        public static HttpClient Client = new HttpClient();
        public static WebClient WebClient = new WebClient();

        public static object lockObject = new object();

        public static int MasksDownloaded = 0;
        public static int MaskErrorCount = 0;
        public static int ImageDownloadErrorCount = 0;



        public static List<ImgData> GetListOfImages()
        {
            string json = Client.GetStringAsync(URL).Result;
            var listOfImages = JsonConvert.DeserializeObject<List<ImgData>>(json);
            return listOfImages;
        }

        public static SampleInfo GetSampleInfo(string id)
        {
            URL = "https://isic-archive.com/api/v1/image";

            string json = Client.GetStringAsync(URL + "/" + id).Result;
            var info = JsonConvert.DeserializeObject<SampleInfo>(json);
            Clinical dataInstance = JObject.Parse(json)["meta"]["clinical"].ToObject<Clinical>();
            info.clinical = dataInstance;
            return info;
        }

        public static List<SampleInfo> GetSampleInfos(List<String> ids)
        {
            try
            {
                URL = "https://isic-archive.com/api/v1/image";
                List<SampleInfo> metadata = new List<SampleInfo>();
                foreach (var id in ids)
                {
                    string json = Client.GetStringAsync(URL + "/" + id).Result;

                    var info = JsonConvert.DeserializeObject<SampleInfo>(json);
                    Clinical dataInstance = JObject.Parse(json)["meta"]["clinical"].ToObject<Clinical>();
                    info.clinical = dataInstance;

                    metadata.Add(info);
                }
                return metadata;
            }
            catch (Exception)
            {
                return new List<SampleInfo>();
            }
        }

        public static bool DownloadImage(string id, string fname_fragment)
        {
            try
            {
                URL = @"https://isic-archive.com/api/v1/image/" + id + "/download";
                string filename = fname_fragment + id + ".jpg";
                WebClient.DownloadFile(URL, filename);
                return true;
            }
            catch (Exception)
            {
                ImageDownloadErrorCount++;
                return false;
            }
        }

        public static List<SegmentationInfo> GetSegmentationInfo(string id)
        {
            List<SegmentationInfo> segmentations = new List<SegmentationInfo>();
            URL = @"https://isic-archive.com/api/v1/segmentation?imageId=";
            string json = Client.GetStringAsync(URL + id).Result;
            var info = JsonConvert.DeserializeObject<List<SegmentationInfo>>(json);
            segmentations.AddRange(info);
            foreach (var item in info)
            {
                item.mole_id = id;
            }
            if (segmentations.Count == 0)
            {
                SegmentationInfo temp = new SegmentationInfo() { mole_id = id };
                segmentations.Add(temp);
            }
            return segmentations;
        }

        public static List<SegmentationInfo> GetSegmentationInfos(List<string> IDs)
        {
            List<SegmentationInfo> segmentations = new List<SegmentationInfo>();
            URL = @"https://isic-archive.com/api/v1/segmentation?imageId=";

            foreach (string id in IDs)
            {
                string json = Client.GetStringAsync(URL + id).Result;
                var info = JsonConvert.DeserializeObject<List<SegmentationInfo>>(json);
                segmentations.AddRange(info);
                foreach (var item in info)
                {
                    item.mole_id = id;
                }
            }

            return segmentations;
        }

        public static bool DownloadSegmetationMask(SegmentationInfo info, string fname_fragment)
        {
            try
            {
                if (info != null && info.created != null)
                {
                    URL = @"https://isic-archive.com/api/v1/segmentation/" + info._id + "/mask";

                    string filename = fname_fragment + info.mole_id + ".jpg";
                    WebClient.DownloadFile(URL, filename);
                    return true;
                }
                UnsuccessfullIDs.Add(info.mole_id);
                return false;
            }
            catch (Exception)
            {
                UnsuccessfullIDs.Add(info.mole_id);
                return false;
            }
        }

        public static void DownloadAllSegmentatioMasks(List<SegmentationInfo> info, string fname_fragment)
        {
            foreach (SegmentationInfo oneseg in info)
            {
                DownloadSegmetationMask(oneseg, fname_fragment);
            }
        }

        public static bool DownloadAllDataById(string id, string fname, string mask_fname)
        {
            DownloadImage(id, fname);
            SampleInfo info = GetSampleInfo(id);
            if (info != null)
            {

                List<SegmentationInfo> seginfo = GetSegmentationInfo(id);
                SegmentationInfo segmentation = seginfo.GroupBy(x => x.mole_id).Select(y => y.First()).FirstOrDefault();
                if (DownloadSegmetationMask(segmentation, mask_fname))
                {
                    MasksDownloaded++;
                }
                else
                {
                    MaskErrorCount++;
                }
                return true;
            }
            else
            {
                MaskErrorCount++;
                UnsuccessfullIDs.Add(id);
                return false;
            }

        }

        public static void CreateDataset(string path, string dest, int count)
        {
            Random rnd = new Random();

            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] files = di.GetFiles("*.jpg");

            FileInfo[] filesToMove = files.OrderBy(x => rnd.Next(0, files.Count() + 1)).Take(count).ToArray();

            foreach (var file in filesToMove)
            {
                string fullpath = Path.GetFullPath(file.FullName);
                File.Move(fullpath, dest + file.Name);
            }
        }
    }
}

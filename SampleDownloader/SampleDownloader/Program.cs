﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDownloader
{
    class Program
    {
        private static string samplePath = @"D:\Akos\OE\szakdoga\Samples\Benign\";
        private static string validationPath = @"D:\Akos\OE\szakdoga\unprocessed\val\";
        private static string testPath = @"D:\Akos\OE\szakdoga\unprocessed\test\";

        private static string malignantPrefix = @"Malignant\";
        private static string benignPrefix = @"Benign\";

        static void Main(string[] args)
        {
            //Download all images and segmentation masks.
            RetrieveImages();

            //Create validation dataset.
            ISICHandler.CreateDataset(samplePath + malignantPrefix, validationPath + malignantPrefix, 250);
            ISICHandler.CreateDataset(samplePath + benignPrefix, validationPath + benignPrefix, 250);

            //Create test dataset
            ISICHandler.CreateDataset(samplePath + malignantPrefix, testPath + malignantPrefix, 90);
            ISICHandler.CreateDataset(samplePath + benignPrefix, testPath + benignPrefix, 90);

            Console.WriteLine("Images and masks downloaded, datasets created.");
            Console.ReadKey();
        }


        public static void GetSummary(int reached, int downloaded, int error)
        {
            Console.WriteLine($"Samples reached: {reached} (successfull: {downloaded}, failed: {error})");
            Console.WriteLine($"Masks to redownload: {ISICHandler.UnsuccessfullIDs.Count}");
        }

        public static void RetrieveImages()
        {
            Console.WriteLine("Collecting image data...");
            List<ImgData> data = ISICHandler.GetListOfImages();
            List<string> imageIds = data.Select(x => x._id).ToList();
            Console.WriteLine("Done. (Image IDs collected.)");

            Console.WriteLine("Collecting sample informations...");
            List<SampleInfo> metadata = ISICHandler.GetSampleInfos(imageIds);
            Console.WriteLine("Metadata collected.");

            List<string> malignant_ids = metadata.Where(x => x.clinical.benign_malignant == "malignant").Select(x => x._id).ToList();
            List<string> benign_ids = metadata.Where(x => x.clinical.benign_malignant == "benign").Select(x => x._id).ToList();
            //malignant_ids.AddRange(benign_ids);
            Console.WriteLine("Done. (Benign and malignant sample IDs collected.)");

            int downloads = 0;
            int samplesReached = 0;
            int errorCount = 0;

            Console.WriteLine("Starting to download samples and masks...");
            
            foreach (string id in malignant_ids)
            {
                //SampleInfo info = metadata.FirstOrDefault(x => x._id == id);
                string fname = @"D:\Akos\OE\szakdoga\Samples\Malignant\";
                string mask_fname = @"D:\Akos\OE\szakdoga\Masks\test2\";
                try
                {
                    Console.SetCursorPosition(0, 19);
                    Console.WriteLine("Samples reached: " + ++samplesReached);
                    if (ISICHandler.DownloadAllDataById(id, fname, mask_fname))
                    {
                        Console.SetCursorPosition(0, 20);
                        Console.WriteLine("Masks dowloaded: " + ISICHandler.MasksDownloaded);
                    }
                    Console.SetCursorPosition(0, 21);
                    Console.WriteLine("Mask download errors: " + ISICHandler.MaskErrorCount + "(" + ISICHandler.UnsuccessfullIDs.Count + ")");
                }
                catch (Exception)
                {
                    ++errorCount;
                    ++ISICHandler.MaskErrorCount;
                }
            }

            Console.Clear();
            Console.WriteLine("Malignant downloads done");
            GetSummary(samplesReached, ISICHandler.MasksDownloaded, ISICHandler.MaskErrorCount);

            foreach (string id in benign_ids)
            {
                string fname = @"D:\Akos\OE\szakdoga\Samples\Benign\";
                string mask_fname = @"D:\Akos\OE\szakdoga\Masks\test\";
                try
                {
                    Console.SetCursorPosition(0, 19);
                    Console.WriteLine("Samples reached: " + ++samplesReached);
                    if (ISICHandler.DownloadAllDataById(id, fname, mask_fname))
                    {
                        Console.SetCursorPosition(0, 20);
                        Console.WriteLine("Masks dowloaded: " + ISICHandler.MasksDownloaded);
                    }
                    Console.SetCursorPosition(0, 21);
                    Console.WriteLine("Mask download errors: " + ISICHandler.MaskErrorCount + "(" + ISICHandler.UnsuccessfullIDs.Count + ")");
                }
                catch (Exception)
                {
                    ++errorCount;
                    ++ISICHandler.MaskErrorCount;
                }
            }

            Console.Clear();
            Console.WriteLine("Benign downloads done");
            GetSummary(samplesReached, ISICHandler.MasksDownloaded, ISICHandler.MaskErrorCount);
        }
    }
}

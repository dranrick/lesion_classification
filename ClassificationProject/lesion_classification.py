import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt
import tensorflow as tf
import keras
from keras import Model, Sequential, layers
from keras.layers import ZeroPadding2D, Convolution2D, Convolution3D, MaxPooling2D, Dense, Flatten, Dropout
from tensorflow.keras import optimizers
from keras.callbacks import EarlyStopping
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.vgg16 import VGG16
from keras.applications.inception_v3 import InceptionV3, preprocess_input, decode_predictions
from keras.metrics import mean_absolute_error
from itertools import chain
from preprocess import resize_image
from tensorflow.keras.preprocessing import image

# A modul valósítja meg a klasszifikációs rendszer (CNN) implementálását. A modulban számos architektúrális kísérlet látható, adataugmentációs módszerekkel kiegészítve.
# Ide tartozik még a súlyok mentése, és betöltése, a modell előkészítése a webalalkalmazáshoz, valamint a fényképek feldolgozás előtti normalizálása is.
# A végezetben felhasznált architektúrát és elért eredményeket a dolgozat 9. és 10. fejezeti taglalják.

IMG_SIZE = 248

IMAGENET_MEAN_PIXEL_VALUES = [103.939, 116.779, 123.680]

EPOCH_COUNT = 3
BATCH_SIZE = 32
LEARNING_RATE = 0.001
DROPOUT_RATE = 0.2
PATIENCE = 1
METRICS = ['accuracy', keras.metrics.mean_absolute_error, keras.metrics.FalsePositives()]

RMSprop = keras.optimizers.RMSprop(learning_rate=LEARNING_RATE)
ADAM = keras.optimizers.Adam(learning_rate=LEARNING_RATE)
SGD = keras.optimizers.SGD(learning_rate=LEARNING_RATE)

OPTIMIZER = RMSprop

TRAIN_DATA_PATH = 'D:/Akos/OE/szakdoga/processed/train'
VAL_DATA_PATH = 'D:/Akos/OE/szakdoga/processed/validation'
BENIGN_TEST_DATA_PATH = 'D:/Akos/OE/szakdoga/processed/test/Benign'
MALIGNANT_TEST_DATA_PATH = 'D:/Akos/OE/szakdoga/processed/test/Malignant'

UNBALANCED_TRAIN_PATH = 'data/unbalanced/train/'
UNBALANCED_VAL_PATH = 'data/unbalanced/test/'

TOP_MODEL_WEIGHTS = 'top_mode_weights.h5'
BASE_MODEL_WEIGHTS = 'weights/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5'
FINE_TUNED_WEIGHTS = 'fine_tuned_weights.h5'

WEIGHTS_PATH = 'weights/weights_no_aug.h5'

class CombinedGen():
    def __init__(self, *gens):
        self.gens = gens

    def generate(self):
        while True:
            for g in self.gens:
                yield next(g)

    def __len__(self):
        return sum([len(g) for g in self.gens])


def load_data_without_augmentation(data_path):
    generator = ImageDataGenerator(rescale=1./255, featurewise_std_normalization=True)
    data = generator.flow_from_directory(data_path,
                                         target_size=(IMG_SIZE, IMG_SIZE),
                                         class_mode='binary',
                                         batch_size=BATCH_SIZE)
    return data


def load_data_with_augmentation(data_path):
    generator = ImageDataGenerator(rescale=1./255,
                                   horizontal_flip=True,
                                   vertical_flip=True,
                                   zoom_range=0.2,
                                   shear_range=0.2,
                                   featurewise_std_normalization=True)

    data = generator.flow_from_directory(data_path,
                                         target_size=(IMG_SIZE, IMG_SIZE),
                                         class_mode='binary',
                                         batch_size=BATCH_SIZE)

    return data


def build_orig_vgg16_cnn():
    from keras.applications.vgg16 import VGG16

    pre_trained_model = VGG16(input_shape=(IMG_SIZE, IMG_SIZE, 3), include_top=False, weights='imagenet', classes=1)

    for layer in pre_trained_model.layers:
        layer.trainable = False

    last_layer = pre_trained_model.get_layer('block5_pool')
    print('output shape', last_layer.output_shape)
    last_output = last_layer.output

    top_model = layers.Flatten()(last_output)
    top_model = layers.Dense(1024, activation='relu')(top_model)
    top_model = layers.Dropout(DROPOUT_RATE)(top_model)
    top_model = layers.Dense(1, activation='sigmoid')(top_model)

    model = Model(pre_trained_model.input, top_model)

    # model.summary()

    model.compile(optimizer=OPTIMIZER, loss='binary_crossentropy',
                  metrics=['accuracy', keras.metrics.mean_absolute_error])


def build_inceptionv3_cnn():
    pre_trained_model = InceptionV3(input_shape=(IMG_SIZE, IMG_SIZE, 3), include_top=False, weights='imagenet')
    # pre_trained_model.summary()

    for layer in pre_trained_model.layers:
        layer.trainable = False

    # pre_trained_model.summary()

    last_layer = pre_trained_model.get_layer('mixed7')
    print('output shape', last_layer.output_shape)
    last_output = last_layer.output

    top_model = layers.Flatten()(last_output)

    top_model = layers.Dense(1024, activation='relu')(top_model)
    top_model = layers.Dropout(DROPOUT_RATE)(top_model)
    top_model = layers.Dense(1, activation='sigmoid')(top_model)

    model = Model(pre_trained_model.input, top_model)
    # model.compile(optimizer='adam', loss='binary_crossentropy',
    #               metrics=['accuracy'])

    model.compile(optimizer=OPTIMIZER, loss='binary_crossentropy',
                  metrics=METRICS)
    return model


def build_custom_vgg16_cnn_base():
    base_model = Sequential()
    # base_model.add(ZeroPadding2D((1, 1), input_shape=(3, imsize, imsize)))
    base_model.add(Convolution2D(64, 3, 3, activation='relu', name='conv1_1'))
    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(64, 3, 3, activation='relu', name='conv1_2'))
    base_model.add(MaxPooling2D((2, 2), padding='same', strides=(2, 2)))

    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(128, 3, 3, activation='relu', name='conv2_1'))
    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(128, 3, 3, activation='relu', name='conv2_2'))
    base_model.add(MaxPooling2D((2, 2), padding='same', strides=(2, 2)))

    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(256, 3, 3, activation='relu', name='conv3_1'))
    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(256, 3, 3, activation='relu', name='conv3_2'))
    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(256, 3, 3, activation='relu', name='conv3_3'))
    base_model.add(MaxPooling2D((2, 2), padding='same', strides=(2, 2)))

    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(512, 3, 3, activation='relu', name='conv4_1'))
    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(512, 3, 3, activation='relu', name='conv4_2'))
    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(512, 3, 3, activation='relu', name='conv4_3'))
    base_model.add(MaxPooling2D((2, 2), padding='same', strides=(2, 2)))

    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(512, 3, 3, activation='relu', name='conv5_1'))
    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(512, 3, 3, activation='relu', name='conv5_2'))
    base_model.add(ZeroPadding2D((1, 1)))
    base_model.add(Convolution2D(512, 3, 3, activation='relu', name='conv5_3'))
    base_model.add(MaxPooling2D((2, 2), padding='same', strides=(2, 2)))

    base_model.compile(optimizer=RMSprop, loss='binary_crossentropy', metrics=['accuracy'])
    base_model.fit(load_data_with_augmentation(TRAIN_DATA_PATH), epochs=1, steps_per_epoch=1)
    base_model.load_weights(BASE_MODEL_WEIGHTS)
    base_model.compile(optimizer=RMSprop, loss='binary_crossentropy', metrics=['accuracy'])
    print(base_model.output_shape)
    return base_model


def proba():
    model = Sequential()
    model.add(Convolution2D(input_shape=(IMG_SIZE, IMG_SIZE, 3), filters=64, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(Convolution2D(filters=64, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Convolution2D(filters=128, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(Convolution2D(filters=128, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Convolution2D(filters=256, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(Convolution2D(filters=256, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(Convolution2D(filters=256, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Convolution2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(Convolution2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(Convolution2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Convolution2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(Convolution2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(Convolution2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    model.compile(optimizer=RMSprop, loss='binary_crossentropy', metrics=['accuracy'])
    return model


def build_custom_vgg16_cnn_top():
    topmodel = Sequential()

    topmodel.add(Flatten(input_shape=(IMG_SIZE, IMG_SIZE, 3)))
    topmodel.add(Dense(256, activation='relu'))
    topmodel.add(Dropout(DROPOUT_RATE))
    topmodel.add(Dense(1, activation='sigmoid'))
    print(topmodel.output_shape)
    return topmodel


def train_vgg16_top_model():
    train_generator = ImageDataGenerator(
        rescale=1. / 255)

    validation_generator = ImageDataGenerator(rescale=1. / 255)

    train_data = train_generator.flow_from_directory(
        TRAIN_DATA_PATH,
        target_size=(IMG_SIZE, IMG_SIZE),
        class_mode='binary')

    validation_data = validation_generator.flow_from_directory(
        VAL_DATA_PATH,
        target_size=(IMG_SIZE, IMG_SIZE),
        class_mode='binary')

    model = build_custom_vgg16_cnn_top()

    model.compile(optimizer=RMSprop, loss='binary_crossentropy', metrics=['accuracy'])

    model.fit_generator(
        train_data,
        epochs=EPOCH_COUNT,
        validation_data=validation_data,
        verbose=1)

    model.save_weights(TOP_MODEL_WEIGHTS, save_format='h5')


def fine_tune_vgg16():
    model = build_custom_vgg16_cnn_base()
    top_model = build_custom_vgg16_cnn_top()

    top_model.load_weights(TOP_MODEL_WEIGHTS)

    model.add(top_model)

    for layer in model.layers[:25]:
        layer.trainable = False

    model.compile(loss='binary_crossentropy',
                  optimizer=optimizers.SGD(lr=LEARNING_RATE, momentum=0.9),
                  metrics=['accuracy'])

    train_datagen = ImageDataGenerator(
            rescale=1./255,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True)

    test_datagen = ImageDataGenerator(rescale=1./255)

    train_generator = train_datagen.flow_from_directory(
            TRAIN_DATA_PATH,
            target_size=(IMG_SIZE, IMG_SIZE),
            batch_size=BATCH_SIZE,
            class_mode='binary')

    validation_generator = test_datagen.flow_from_directory(
            VAL_DATA_PATH,
            target_size=(IMG_SIZE, IMG_SIZE),
            batch_size=BATCH_SIZE,
            class_mode='binary')

    # fine-tune the model
    model.fit_generator(
            train_generator,
            validation_data=validation_generator)

    # save the fine-tuned model weights
    model.save_weights(FINE_TUNED_WEIGHTS, save_format='h5')


def build_custom_vgg16_cnn():
    base = build_custom_vgg16_cnn_base()
    top = build_custom_vgg16_cnn_top()
    base.add(top)

    base.load_weights(FINE_TUNED_WEIGHTS)

    return base


def normalize_image(img):
    img = cv.resize(img, (IMG_SIZE, IMG_SIZE))
    img = img.astype(np.float32, copy=False)

    for x in range(3):
        img[:, :, x] = img[:, :, x] - IMAGENET_MEAN_PIXEL_VALUES[x]

    img = img.transpose((1, 0, 2))
    img = np.expand_dims(img, axis=0)
    return img


def test_model_for_testdata(model):
    benign_test_data = load_data_without_augmentation(BENIGN_TEST_DATA_PATH)
    malignant_test_data = load_data_without_augmentation(MALIGNANT_TEST_DATA_PATH)

    benign_results = model.predict(benign_test_data)
    benign_preds = np.round(benign_results, 0)
    correct_benign_preds = (benign_preds.size - np.count_nonzero(benign_preds))

    malignant_results = model.predict(malignant_test_data)
    malignant_preds = np.round(malignant_results, 0)
    correct_malignant_preds = np.count_nonzero(malignant_preds)

    print("***********************************************")
    print("RESULTS: ")
    print("Correct benign preds: " + str(correct_benign_preds) + " / " + str(benign_preds.size))
    print("Correct malignant preds: " + str(correct_malignant_preds) + " / " + str(malignant_preds.size))

    print(benign_results, benign_preds)
    return None


def plot_training_results(hist):
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.plot(hist.history["loss"], 'r', label="Loss")
    plt.plot(hist.history["accuracy"], 'b', label="Accuracy")
    plt.plot(hist.history["val_accuracy"], 'g', label="Validation accuracy")
    # plt.plot(hist.history["val_mean_absolute_error"], 'k')
    plt.show()


def classify_image(model, img):
    normalized = normalize_image(img)
    normalized_prediction = model.predict(normalized)

    # print(normalized_prediction)

    # img_without_normalization = cv.resize(img, (IMG_SIZE, IMG_SIZE))
    img_without_normalization = resize_image(img)  # 248 x 248
    img_without_normalization = img_without_normalization.transpose((1, 0, 2))
    img_without_normalization = np.expand_dims(img_without_normalization, axis=0)

    prediction = model.predict(img_without_normalization)

    return prediction


def train_cnn(model):
    train_data = load_data_without_augmentation(TRAIN_DATA_PATH)
    validation_data = load_data_without_augmentation(VAL_DATA_PATH)

    from keras.callbacks import TensorBoard
    tbcb = TensorBoard(log_dir="./logs/MobileNetV2-default")

    es = EarlyStopping(monitor='val_loss', min_delta=0, patience=PATIENCE)

    hist = model.fit_generator(train_data,
                               epochs=EPOCH_COUNT,
                               validation_data=validation_data,
                               callbacks=[es],
                               verbose=1
                               )

    model.save_weights("weights/random_proba", False, save_format='h5')
    plot_training_results(hist)

    return model


def setup_model_for_webapp():
    model = build_inceptionv3_cnn()
    load_model_weights(model)
    return model


def load_model_weights(model):
    model.load_weights(WEIGHTS_PATH)
    model.compile(optimizer=OPTIMIZER, loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model


def save_model(model):
    tf.saved_model.save(model, 'models/lc')


def main():
    classify = True
    train = False
    mode = 0 # 0 - classify, 1 - train

    model = build_inceptionv3_cnn()
    # model.summary()

    if mode == 0:
        load_model_weights(model)
        img = cv.imread(filename="test.jpg")
        # classify_image(model, img)
        classify_image(model, img)

    if mode == 1:
        train_cnn(model)
        plot_training_results(model)
        test_model_for_testdata(model)


if __name__ == "__main__":
    main()

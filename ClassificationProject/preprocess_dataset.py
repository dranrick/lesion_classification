import cv2 as cv
import numpy as np
import os

IMSIZE = 256

def rotate_image_clockwise(img):
    if img.shape[0] > img.shape[1]:
        img = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)
    border_cropped = img[1:-1, 1: -1, :]
    return border_cropped

def rotate_image_clockwise_2(original):
    rows, cols, colors = original.shape
    if rows > cols:
        orig = np.zeros([cols, rows, colors])
        for f in range(original.shape[2]):
            orig[:, :, f] = np.fliplr(original[:, :, f].T)
        orig = np.array(orig[1:-1, 1:-1],
                        'uint8')
    else:
        orig = original[1:-1, 1:-1]
    return original

def resize_image(img):
    temp_size = img.shape[1]
    temp_img = np.zeros((temp_size, temp_size, img.shape[2]), dtype=np.uint8)
    temp_img[0:img.shape[0], 0:img.shape[1], :] = img
    resized = cv.resize(temp_img, (IMSIZE, IMSIZE), interpolation=cv.INTER_AREA)
    return resized

def apply_existing_segmentation_mask(img, mask):
    img = cv.bitwise_and(img, img, mask=mask)
    return img

def incrase_contrast(img):
    alpha = 1.6
    beta = 1.1

    img = alpha * img + beta
    img = np.clip(img, 0, 255).astype(np.uint8)
    return img

def preprocess(img_path, mask_path, dest):
    index = 0
    extension = ".jpg"

    for filename in os.listdir(img_path):
        full_img_path = os.path.join(img_path, filename)
        full_mask_path = os.path.join(mask_path, filename)
        img = cv.imread(full_img_path)
        mask = cv.imread(full_mask_path)

        result = apply_existing_segmentation_mask(img, mask)
        result = rotate_image_clockwise(result)
        result = resize_image(result)

        fullpath = dest + "/" + filename + str(index) + extension
        cv.imwrite(fullpath, result)
        index = index + 1
import cv2 as cv
import numpy as np

IMSIZE = 248

# A modul a tanítóminták számosságának növelését kívánja megoldani, adatgenerálás és augmentálás segítségével. Bár egyelőre még csak tervként szerepel,
# de a modul célja az egyes léziókba írható legnagyobb négyzet megkeresése, majd forgatása lesz, ezáltal színesítve az adatbázist.
# A modul nem szerepel a dolgozatban, ugyanis jelenleg kezdetleges állapotban van, bemutatásra még nem alkalmas, illetve nem képezi szigorúan a dolgozat témájának részét.

def rotate_image_clockwise_2(original):
    rows, cols, colors = original.shape
    if rows > cols:
        orig = np.zeros([cols, rows, colors])
        for f in range(original.shape[2]):
            orig[:, :, f] = np.fliplr(original[:, :, f].T)
        orig = np.array(orig[1:-1, 1:-1],
                        'uint8')
    else:
        orig = original[1:-1, 1:-1]
    return original

def rotate_image_clockwise(img):
    if img.shape[0] > img.shape[1]:
        img = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)
    border_cropped = img[1:-1, 1: -1, :]
    return border_cropped

def resize_image(img):
    temp_size = img.shape[1]
    temp_img = np.zeros((temp_size, temp_size, img.shape[2]), dtype=np.uint8)
    temp_img[0:img.shape[0], 0:img.shape[1], :] = img
    resized = cv.resize(temp_img, (248, 248), interpolation=cv.INTER_AREA)
    return resized

def locate_mole(img):
    # closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel, iterations=2)
    # blur = cv2.blur(closing, (15, 15))

    gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    FILT_THRESH = np.max((150, np.max(gray_img) + np.min(gray_img) - np.mean(gray_img))) + np.std(
        gray_img) / 10
    _, thresh = cv.threshold(gray_img, FILT_THRESH, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)

    contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
    cnt = max(contours, key=cv.contourArea)

    height, width = img.shape[0:2]
    mask = np.zeros((height, width), np.uint8)

    cv.drawContours(mask, [cnt], -1, 255, -1)
    res = cv.bitwise_and(img, img, mask=mask)

    return res

def locate_rectangle_outside_image(img):
    imgray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    ret, thresh = cv.threshold(imgray, 50, 255, cv.THRESH_BINARY)
    # find contours
    contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    cnt = contours[0]
    # straight rectangle
    x, y, w, h = cv.boundingRect(cnt)
    ((x,y), (w,h), angle) = cv.minAreaRect(cnt)
    img = cv.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
    return img


def largest_rect_inside_mole(mask):
    topleft_corner = []
    bottomright_corner = []
    rectangle_heights = []
    rectangle_areas = []
    # lets start scanning from the left
    # and find the outer two points in each vertical line
    for i in range(0, mask.shape[1]):
        line = mask[:, i]
        foreground_indecies = np.where(line == 255)

        top_p1 = foreground_indecies[0][0]
        bottom_p1 = foreground_indecies[0][-1]
        line1_length = bottom_p1 - top_p1
        # scan the mask fromt the right side
        for j in range(mask.shape[1] - 1, i + 2, -1):
            line2 = mask[:, j]
            foreground_indecies = np.where(line2 == 255)
            top_p2 = foreground_indecies[0][0]
            bottom_p2 = foreground_indecies[0][-1]
            # find length of right line
            line2_length = bottom_p2 - top_p2

            # If the two lines are equal then we have an upright rectangle  :)
            if line1_length == line2_length and i != j:
                topleft_corner.append([i, top_p1])
                bottomright_corner.append([j, bottom_p2])
                rectangle_heights.append(line1_length)
                rectangle_areas.append((j - i) * (line1_length))

    # Now we have the list of all possible rectangle heights and their correpsonding pionts
    # You can then decide how to choose the right rectangle
    # A - By how far it extends vertically
    # B - By area size
    # I am going to demonstrate how to do B
    max_area_index = np.argmax(np.array(rectangle_areas))
    topleft_pt = tuple(topleft_corner[max_area_index])
    bottomright_pt = tuple(bottomright_corner[max_area_index])
    rgb_mask = cv.cvtColor(mask, cv.COLOR_GRAY2BGR)
    color = (255, 0, 0)
    # Line thickness of 2 px
    thickness = 2
    # Using cv2.rectangle() method
    # Draw a rectangle with blue line borders of thickness of 2 px
    image = cv.rectangle(rgb_mask, topleft_pt, bottomright_pt, color, thickness)
    return image

def main():
    img = cv.imread("image.jpg")
    img = locate_rectangle_outside_image(img)
    img = resize_image(img)
    cv.imshow('result', img)
    cv.waitKey(0)

if __name__ == "__main__":
    main()
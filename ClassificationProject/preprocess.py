import numpy as np
from scipy import signal
import copy
import os
import cv2 as cv

IMG_PATH = ''
MASK_PATH = ''

ALPHA_VAL = 1.6
BETA_VAL = 1.1

VGN_DETECTION_RAD = 0.7
VGN_MASK_TRHESH = 10
VGN_CROP_TRHESH = 93

ROI_TOPLEFT_ARG = 0.05
ROI_BOTTOMRIGHT_ARG = 0.05

GAUSS_BLUR_DIM = 5

DIM = 256

# A modul az egyes fényképek előfeldolgozását végzi és egyaránt felelős a tanítóminták, valamint a prediktálni kívánt minták feldolgozásáért.
# Az műveletek végrehajtására több függvény is készült, azzal a célzattal, hogy megtalájam az adatokhoz leginkább illeszkedő megoldást.
# A teljes feldolgozási folyamat megfelel a szakdolgozat 8. fejezetében leírtaknak.



def LoadImagesFromFolder(folder): # A fgv. beolvassa a megadott mappa teljes tartalmát
    images = []
    for filename in os.listdir(folder):
        img = cv.imread(os.path.join(folder, filename))
        if img is not None:
            images.append(img)
    return images


def SaveProcessedImagesToFolder(Folder, Images): # A fgv. elmenti a megadott fényképeket a megadott mappába
    filename = "img_processed"
    extension = ".jpg"
    index = 0

    for img in Images:
        fullpath = Folder + "/" + filename + str(index) + extension
        cv.imwrite(fullpath, img)
        index = index + 1


def equalize_hist(Image): #A fgv. hisztogram kiegyenlítést végez a paraméterként megadott képen.
    return cv.equalizeHist(Image)


def incrase_contrast(img): #A függvény megnöveli a paraméterként megadott fénykép kontrasztját az előre beállított értékkel.
    alpha = 1.6
    beta = 1.1

    img = alpha * img + beta
    img = np.clip(img, 0, 255).astype(np.uint8)
    return img


def vignette_detection_by_mask(img): # maszk segítségével történő detektálása az ún. vigetting effektnek.
    height, width, channels = img.shape

    centerX = int(width / 2)
    centerY = int(height / 2)
    radius = int(height * 0.7)

    mask = np.ones(img.shape, dtype=np.uint8)
    mask = cv.circle(mask, (centerX, centerY), radius, (255, 255, 255), -1)

    mask = (255 - mask)

    result = cv.bitwise_and(img, mask)

    averageInstensity = np.mean(result)

    return (averageInstensity < VGN_MASK_TRHESH)


def vignette_detection_by_cropping(img): # Az ún. vigentting effekt jelenlétének meghatározása vágási művelet segítségével.
    roi_topleft_arg = ROI_TOPLEFT_ARG
    roi_bottomright_arg = ROI_BOTTOMRIGHT_ARG

    height, width, channels = img.shape

    roi_topleft_width = int(roi_topleft_arg * width)
    roi_topleft_height = int(roi_topleft_arg * height)

    temp_topleft = img[0:roi_topleft_height, 0:roi_topleft_width]

    roi_bottomright_width = int((1 - roi_bottomright_arg) * width)
    roi_bottomright_height = int((1 - roi_bottomright_arg) * height)
    temp_bottomright = img[roi_bottomright_height: roi_bottomright_height + roi_topleft_height,
                       roi_bottomright_width: roi_bottomright_width + roi_topleft_width]

    avg_tl = np.mean(temp_topleft)
    avg_br = np.mean(temp_bottomright)

    avg = (avg_tl + avg_br) / 2

    return (avg <= VGN_CROP_TRHESH)


def detect_vignetting(img): #A fenti két függvény összekapcsolására használatos ellenőrző függvény
    if (vignette_detection_by_cropping(img)): # and / or
        return True
    return False


def reduce_noise_by_blurring(image): # Gauss simítás
    return cv.GaussianBlur(image, (GAUSS_BLUR_DIM, GAUSS_BLUR_DIM), 0)


def createGaussianMask(length, rad=3): #Gauss maszk létrehozásá 1. mód
    dim_x = np.arange(0, length, 1, float)
    dim_y = dim_x[:, np.newaxis]

    center_x = center_y = length // 2

    gauss = np.exp(-4 * np.log(2) * ((dim_x - center_x) ** 2 + (dim_y - center_y) ** 2) / rad ** 2)
    return gauss


def gaussian_kernel(kernlen=21, std=3): #Gauss maszk létrehozásá 2. mód
    gkern1d = signal.gaussian(kernlen, std=std).reshape(kernlen, 1)
    gkern2d = np.outer(gkern1d, gkern1d)
    return gkern2d


def determine_crop_radius(img): #Vágási rádiusz meghatározása
    idx = 0
    center_x = img.shape[0] / 2

    for y in range(img.shape[1]):
        if (img[center_x, y] <= 10):
            idx = idx + 1

    idx = idx / 2
    print('idx: ' + str(idx))
    return idx


def crop_image(img): #A vignetting effekt eltávolítása a kép köralakú maszkolásával. 1. módszer
    height, width, ch = np.shape(img)
    center_x = width // 2
    center_y = height // 2

    roi = np.where(img[center_x, :] > 50)

    # radius = (np.max(roi) - np.min(roi)) // 2
    radius = np.min([center_x, center_y])
    # radius = determine_crop_radius(img)

    gauss_matrix = createGaussianMask(width, radius)
    # gauss_matrix = matlab_style_gauss2D((width, width), radius)
    # gauss_matrix = gkern(width, radius)

    cv.imshow('Eredeti gauss maszk', gauss_matrix)

    mask_x = (width - height) // 2
    mask_y = mask_x + height
    circle_mask = gauss_matrix[mask_x:mask_y, :]
    circle_mask[circle_mask < 0.095] = 0
    circle_mask[circle_mask > 0.095] = 1

    # cv.imshow('Kuszobolt Gauss maszk', circle_mask)

    result = np.multiply(img, circle_mask[0:img.shape[0], :, None])
    result[result == 0] = 255
    result = np.array(result, 'uint8')

    return result


def crop_image_2(img): #A vignetting effekt eltávolítása a kép köralakú maszkolásával. 2. módszer
    mask = np.zeros(shape=img.shape[0:2], dtype=np.uint8)

    height, width, channels = img.shape
    center_x = width // 2
    center_y = height // 2

    roi = np.where(img[center_x, :] > 50)  # a feltehetően értékes értékek kigyűjtése, x fix -y tengely közepén vagyunk
    radius = np.min([center_x, center_y])
    # radius = (np.max(roi) - np.min(roi)) // 2

    mask = cv.circle(mask, (center_x, center_y), radius, (255, 255, 255), -1)
    # mask = 255 - mask
    # cv.imshow('mask', mask)

    result = np.bitwise_and(img, mask[:,:,None])
    result[result == 0] = 255
    return result


def crop_image_3(img, radiusOption=0): #A vignetting effekt eltávolítása a kép köralakú maszkolásával. 3. módszer
    img_size = np.shape(img)
    center = (img_size[1] // 2, img_size[0] // 2)
    use_thresh = 0.02
    if radiusOption == 0:
    
        find_black_circle = np.where(img[img_size[0] // 2, :] > 50)
        c1 = np.min(find_black_circle)
        c2 = np.max(find_black_circle)

        if c1 > 20:
            radius = (c2 - c1) // 2
            use_thresh = .095
        else:
            radius = np.min(center)

    else:
        radius = np.max(center) * 1.1

    gauss_hump = createGaussianMask(np.max(img_size), radius)
    py1 = (gauss_hump.shape[0] - np.min(img_size)) // 2
    py2 = py1 + np.min(img_size[0:2])
    print(py1)
    print(py2)
    circle_mask = gauss_hump[py1:py2, :]

    circle_mask[circle_mask < use_thresh] = 0
    circle_mask[circle_mask >= use_thresh] = 1

    circle_crop = np.multiply(circle_mask[:,:,None], img[:,:,np.newaxis])
    circle_crop[circle_crop == 0] = 255
    circle_crop = np.array(circle_crop,
                           'uint8')

    return circle_crop


def rotate_image_clockwise(img): #A fénykép elforgatása 1. módszer
    if img.shape[0] > img.shape[1]:
        img = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)
    border_cropped = img[1:-1, 1: -1, :]
    return border_cropped


def rotate_image_clockwise_2(original): #A fénykép elforgatása 2. módszer
    rows, cols, colors = original.shape
    if rows > cols:
        orig = np.zeros([cols, rows, colors])
        for f in range(original.shape[2]):
            orig[:, :, f] = np.fliplr(original[:, :, f].T)
        orig = np.array(orig[1:-1, 1:-1],
                        'uint8')
    else:
        orig = original[1:-1, 1:-1]
    return original


def locate_mole(img): # A lézió szegmentálás küszöböléssel 1. módszer
    # closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel, iterations=2)
    # blur = cv2.blur(closing, (15, 15))

    gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    FILT_THRESH = np.max((150, np.max(gray_img) + np.min(gray_img) - np.mean(gray_img))) + np.std(
        gray_img) / 10
    _, thresh = cv.threshold(gray_img, FILT_THRESH, 255, cv.THRESH_BINARY_INV + cv.THRESH_OTSU)

    contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
    cnt = max(contours, key=cv.contourArea)

    height, width = img.shape[0:2]
    mask = np.zeros((height, width), np.uint8)

    cv.drawContours(mask, [cnt], -1, 255, -1)
    res = cv.bitwise_and(img, img, mask=mask)

    return res


def locate_mole_2(img): # a lézió szegmentálása küszöböléssel 2. módszer
    gray_crop = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    FILT_THRESH = np.max((150, np.max(gray_crop) + np.min(gray_crop) - np.mean(gray_crop))) + np.std(
        gray_crop) / 10

    thresh = cv.threshold(gray_crop, FILT_THRESH, 255, 0)[
        1]

    lesion = copy.deepcopy(img)
    lesion[thresh == 255] = (0, 0, 0)
    lesion = cv.cvtColor(lesion, cv.COLOR_BGR2RGB)

    lesion_gray = cv.cvtColor(lesion, cv.COLOR_RGB2GRAY)
    lesion_edges = cv.Canny(lesion_gray, 100, 200)

    # cv.imshow('processed', lesion)
    # cv.imshow('processed2', lesion_gray)
    # cv.imshow('processed3', lesion_edges)
    # cv.waitKey(0)


def resize_image(img): # A fénykép átméretezése, hogy megfelelő input formába kerüljön
    temp_size = img.shape[1]
    temp_img = np.zeros((temp_size, temp_size, img.shape[2]), dtype=np.uint8)
    temp_img[0:img.shape[0], 0:img.shape[1], :] = img
    resized = cv.resize(temp_img, (248, 248), interpolation=cv.INTER_AREA)
    return resized


def apply_existing_segmentation_mask(img, mask): #Már létező szegmentációs maszk applikálása egy mintára
    # mask = cv.cvtColor(mask, cv.COLOR_BGR2GRAY)
    img = cv.bitwise_and(img, img, mask=mask[:,:,0])
    return img


def process_images(images): #Minden beolvasott fénnykép feldolgozása
    processed_images = []
    for img in images:
        img = rotate_image_clockwise(img) # csak nem fekvő-tájolású képek esetén
        if (detect_vignetting(img)):
            img = crop_image_3(img)
        img = incrase_contrast(img)
        img = reduce_noise_by_blurring(img)
        img = locate_mole(img)
        img = resize_image(img)

        processed_images.append(img)
    return processed_images


def process_image(img): #Egy konkrét fénykép feldolgozása
    # result = incrase_contrast(img)
    result = reduce_noise_by_blurring(img)
    if (detect_vignetting(result)):
        result = crop_image_2(result)
    result = locate_mole(result)
    result = rotate_image_clockwise(result) # csak nem fekvő-tájolású képek esetén
    result = resize_image(result)
    return result


def test_function(): #Teszt függvény az előfeldolgozás folyamatára vonatkozóan
    img = cv.imread('D:/Akos/OE/szakdoga/Samples/Benign/5436e3abbae478396759f0cf.jpg')
    cv.imshow('raw', img)
    cv.waitKey(0)

    img = rotate_image_clockwise(img)
    if (detect_vignetting(img)):
        img = crop_image(img)
    img = incrase_contrast(img)
    img = reduce_noise_by_blurring(img)
    img = locate_mole(img)
    # img = resize_image(img)
    cv.imshow('result', img)
    cv.waitKey(0)


def preprocess_traindata(img_path, mask_path, dest): #A tanítóminták feldolgozását vezérlő függvény.
    index = 0
    extension = ".jpg"

    for filename in os.listdir(img_path):
        full_img_path = os.path.join(img_path, filename)
        full_mask_path = os.path.join(mask_path, filename)
        img = cv.imread(full_img_path)
        mask = cv.imread(full_mask_path)

        if mask is None:
            result = process_image(img)
            fullpath = dest + "/generated/" + filename + str(index) + extension
        else:
            if img.shape[0] == mask.shape[1]:
                img = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)
            result = reduce_noise_by_blurring(img)
            result = apply_existing_segmentation_mask(img, mask)
            result = rotate_image_clockwise(result)
            result = resize_image(result)
            fullpath = dest + "/used/" + filename + str(index) + extension

        print('image ' + str(index) + '# done')
        cv.imwrite(fullpath, result)
        index = index + 1


def main():
    # img_path = 'D:/Akos/OE/szakdoga/Samples/Malignant'
    # mask_path = 'D:/Akos/OE/szakdoga/Masks/test'
    # destination = 'D:/Akos/OE/szakdoga/processed/benign'
    # preprocess_traindata(img_path, mask_path, destination)

    img_path = 'D:/Akos/OE/szakdoga/Samples/Benign'
    mask_path = 'D:/Akos/OE/szakdoga/Masks/test'
    destination = 'D:/Akos/OE/szakdoga/processed/malignant'
    preprocess_traindata(img_path, mask_path, destination)


if __name__ == "__main__":
    main()

# imagesToProcess = LoadImagesFromFolder("D:/Akos/OE/szakdoga/samples/malignant")
# print(len(imagesToProcess))
# imagesToProcess = process_images(imagesToProcess)
# SaveProcessedImagesToFolder("D:/Akos/OE/szakdoga/Actual/data/processed/unbalanced/train/malignant", imagesToProcess)

# preprocess_traindata()

# test_function()
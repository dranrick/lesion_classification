from flask import Flask, flash, url_for, redirect, render_template
from flask import render_template, url_for, request, redirect
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_wtf import FlaskForm
from wtforms import (Form, SubmitField)
from flask_uploads import configure_uploads, IMAGES, UploadSet
from werkzeug.utils import secure_filename
import werkzeug as wz
import lesion_classification as lc
from preprocess import process_image, resize_image
import os
import cv2

# A modul az alkalmazás vizualizációjáért és internetes elérhetőségéért felel.
# A webalkalmazás egyelőre kizárólag demonstrációs célokat szolgál, nyilvánosan nem elérhető, jelen formájában nem alkalmas magasszámú kérések ksizolgálására.
# Az architektúrális fejlesztéseket követően célom egy teljeskörűen felhasználható webszolgáltatás tervezése és megvalósítása is.

app = Flask(__name__)

IMAGES_FOLDER = 'static/uploads/'

SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

app.config['UPLOAD_FOLDER'] = IMAGES_FOLDER
app.config['PROCESSED_FOLDER'] = 'static/processed/'


def classify(model, img):
    prediction = lc.classify_image(model, img)
    return 'Malignant' if (prediction == '[[1.]]') else 'Benign'


class UploadForm(FlaskForm):
    file = FileField()


# @app.route('/home', methods=['GET', 'POST'])
# def home():
#     return render_template('home.html')


@app.route('/home', methods=['GET', 'POST'])
def upload():
    form = UploadForm()

    if form.validate_on_submit():
        uploaded_img_filename = secure_filename(form.file.data.filename)
        uploaded_img_filepath = os.path.join(app.config['UPLOAD_FOLDER'], uploaded_img_filename)
        form.file.data.save(uploaded_img_filepath)

        img_to_classify = cv2.imread(uploaded_img_filepath)

        processed_img = process_image(img_to_classify)
        processed_path = os.path.join(app.config['PROCESSED_FOLDER'], uploaded_img_filename)
        cv2.imwrite(processed_path, processed_img)

        return render_template('prediction.html', input=classify(model, img_to_classify),  user_image=uploaded_img_filepath, processed_image=processed_path)

    return render_template('upload.html', form=form)


model = lc.setup_model_for_webapp()
app.run(host='0.0.0.0', port=50000)